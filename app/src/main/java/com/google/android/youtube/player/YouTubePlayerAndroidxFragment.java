package com.google.android.youtube.player;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import com.google.android.youtube.player.internal.ab;
import com.google.android.youtube.player.YouTubePlayer.OnInitializedListener;
import com.google.android.youtube.player.YouTubePlayer.Provider;



public class YouTubePlayerAndroidxFragment extends Fragment implements Provider {
    private static final String KEY_PLAYER_VIEW_STATE = "YouTubePlayerAndroidxFragment.KEY_PLAYER_VIEW_STATE";

    private Bundle bundle;
    private YouTubePlayerView youTubePlayerView;
    private String developerKey;
    private OnInitializedListener onInitializedListener;

//    public static YouTubePlayerAndroidxFragment newInstance() {
//        return new YouTubePlayerAndroidxFragment();
//    }

    public YouTubePlayerAndroidxFragment() {
    }

    public YouTubePlayerAndroidxFragment(int contentLayoutId) {
        super(contentLayoutId);
    }

    @Override
    public void initialize(String developerKey, OnInitializedListener onInitializedListener) {
        Log.e("yuli", "initialize: 1" );

        this.developerKey = ab.a(developerKey, "Developer key cannot be null or empty");
        Log.e("yuli", "initialize: 2" );

        this.onInitializedListener = onInitializedListener;
        Log.e("yuli", "initialize: 3" );

        initYoutubePlayerView();
        Log.e("yuli", "initialize: 4" );

    }
    private void initYoutubePlayerView(){
        Log.e("yuli", "initYoutubePlayerView: 1" );
        if (youTubePlayerView != null && onInitializedListener != null) {
            Log.e("yuli", "initYoutubePlayerView: 2" );

            youTubePlayerView.a(false);
            Log.e("yuli", "initYoutubePlayerView: 3" );

            youTubePlayerView.a(getActivity(), this, developerKey, onInitializedListener, bundle);
            Log.e("yuli", "initYoutubePlayerView: 4" );

            bundle = null;
            onInitializedListener = null;
            Log.e("yuli", "initYoutubePlayerView: 5" );

        }
        Log.e("yuli", "initYoutubePlayerView: 6" );

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState!=null){
            bundle =savedInstanceState.getBundle(KEY_PLAYER_VIEW_STATE);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        youTubePlayerView = new YouTubePlayerView(getActivity(), null, 0, new OnYoutubeInitializedListener());
        initYoutubePlayerView();
        return youTubePlayerView;
    }

    @Override
    public void onStart(){
        super.onStart();
        if(youTubePlayerView!=null){
            youTubePlayerView.a();
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        if(youTubePlayerView!=null){
            youTubePlayerView.b();
        }
    }

    @Override
    public void onPause(){
        if(youTubePlayerView!=null){
            youTubePlayerView.c();
        }
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState){
        if(youTubePlayerView!=null){
            if(youTubePlayerView.e()!=null){
                Bundle bundle = youTubePlayerView.e();
                outState.putBundle(KEY_PLAYER_VIEW_STATE,bundle);
            }

        }
    }

    @Override
    public void onStop(){
        if(youTubePlayerView!=null){
            youTubePlayerView.d();
        }
        super.onStop();
    }

    @Override
    public void onDestroyView(){
        if(youTubePlayerView!=null){
            if(getActivity()!=null){
                youTubePlayerView.c(getActivity().isFinishing());
            }else{
                youTubePlayerView.c(false);
            }
        }
        youTubePlayerView = null;
        super.onDestroyView();
    }

    @Override
    public void onDestroy(){
        if(youTubePlayerView!=null){
            if(getActivity()!=null){
                youTubePlayerView.b(getActivity().isFinishing());
            }else{
                youTubePlayerView.b(true);
            }
        }
        super.onDestroy();
    }



    private class OnYoutubeInitializedListener implements YouTubePlayerView.b {
        @Override
        public void a(YouTubePlayerView youTubePlayerView, String s, OnInitializedListener onInitializedListener) {
            initialize(s, onInitializedListener);
        }

        @Override
        public void a(YouTubePlayerView youTubePlayerView) {

        }
    }
}
