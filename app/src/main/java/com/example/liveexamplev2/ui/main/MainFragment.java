package com.example.liveexamplev2.ui.main;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.backup.CustomerOnInitializedListener;
import com.backup.CustomerYoutubePlayView;
import com.example.liveexamplev2.R;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;

public class MainFragment extends Fragment {

    private MainViewModel mViewModel;
    private YouTubePlayer youTubePlayer;
    CustomerYoutubePlayView customerYoutubePlayView;
    Button button1;
    Button button2;

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.main_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        // TODO: Use the ViewModel

    }
    @Override
    public void onResume() {
        super.onResume();
        customerYoutubePlayView = (CustomerYoutubePlayView) getActivity().findViewById(R.id.you_tube_player_view);
        customerYoutubePlayView.setOnInitializedListener(new CustomerOnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player, Boolean wasRestored) {
                youTubePlayer =player;
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult result) {
                youTubePlayer = null;
            }
        });
        customerYoutubePlayView.initializePlayer();
        button1 = getActivity().findViewById(R.id.button1);
        button2 = getActivity().findViewById(R.id.button2);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customerYoutubePlayView.initializePlayer();
                if(youTubePlayer!=null){
                    youTubePlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.CHROMELESS);
                    youTubePlayer.loadVideo("6_XXTCHeTp4");
                    youTubePlayer.play();
                }

            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customerYoutubePlayView.initializePlayer();
                if(youTubePlayer!=null){
                    youTubePlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.CHROMELESS);
                    youTubePlayer.loadVideo("QIqV8ul1hXg");
                    youTubePlayer.play();
                }

            }
        });
    }
}