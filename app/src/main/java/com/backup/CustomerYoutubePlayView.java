package com.backup;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;

import com.example.liveexamplev2.R;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerAndroidxFragment;

public class CustomerYoutubePlayView extends FrameLayout {
    private Context context;
    private AttributeSet attrs;
    private int defStyle ;
    private String videoId;
    private String fragmentName;
    private YouTubePlayer player;
    public CustomerOnInitializedListener onInitializedListener;
    private FragmentManager fragmentManager;
    private YouTubePlayerAndroidxFragment youTubePlayerAndroidxFragment = new YouTubePlayerAndroidxFragment();

    public CustomerYoutubePlayView(Context context, @Nullable AttributeSet attrs){
        super(context, attrs);
        this.context = context;
        this.attrs = attrs;
        this.defStyle = 0;
        init();
    }
    public CustomerYoutubePlayView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        this.attrs = attrs;
        this.defStyle = defStyleAttr;
        init();
    }

    private void init() {
        LayoutInflater.from(context).inflate(R.layout.view_prnd_you_tube_player, this, true);
        if(attrs!=null){
            TypedArray typedArray =  context.obtainStyledAttributes(attrs, R.styleable.CustomerYoutubePlayView, defStyle, 0);
            videoId = typedArray.getString(R.styleable.CustomerYoutubePlayView_videoId);
            fragmentName = typedArray.getString(R.styleable.CustomerYoutubePlayView_fragment);
            typedArray.recycle();

        }

        fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.fragment_container, youTubePlayerAndroidxFragment)
                .commitAllowingStateLoss();

//        if (videoId != null) {
//            play(videoId);
//        }
    }

    private FragmentManager getFragmentManager(){
        FragmentManager activityFragmentManager = getFragmentActivity().getSupportFragmentManager();
        if(fragmentName==null){
            return activityFragmentManager;
        }else {
            for(Fragment fragment:activityFragmentManager.getFragments()){
                if(fragment.getClass().getSimpleName().equals(fragmentName)){
                    return fragment.getChildFragmentManager();
                }
            }
        }
        throw new IllegalArgumentException("[$fragmentName] can not found. Please check your fragment name");

    }

    private FragmentActivity getFragmentActivity(){
        Context targetContext = getContext();
        Activity targetActivity = null;
        while(targetContext instanceof ContextWrapper){
            if(targetContext instanceof Activity){
                targetActivity = (Activity) targetContext;
                break;
            }
            targetContext = ((ContextWrapper) targetContext).getBaseContext();
        }
        if(targetActivity instanceof FragmentActivity){
            return (FragmentActivity) targetActivity;
        }else{
            throw new IllegalArgumentException("You have to extend FragmentActivity or AppCompatActivity");
        }
    }

    public void initializePlayer(){
        youTubePlayerAndroidxFragment.initialize(CustomerYoutubePlayView.class.getSimpleName(), new YouTubePlayer.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean wasRestored) {
                if(youTubePlayer==null){
                    return ;
                }else{
                    player = youTubePlayer;
                }
//                if (!wasRestored) {
//                    youTubePlayer.cueVideo(videoId);
//                }
                if(onInitializedListener!=null){
                    onInitializedListener.onInitializationSuccess(provider,youTubePlayer,wasRestored);
                }
//                youTubePlayer.loadVideo(videoId);
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
                if(onInitializedListener!=null){
                    onInitializedListener.onInitializationFailure(provider,youTubeInitializationResult);
                }
            }
        });
    }

    public void setOnInitializedListener(CustomerOnInitializedListener onInitializedListener) {
        this.onInitializedListener = onInitializedListener;
    }

    public YouTubePlayerAndroidxFragment getYouTubePlayerAndroidxFragment() {
        return youTubePlayerAndroidxFragment;
    }
}
