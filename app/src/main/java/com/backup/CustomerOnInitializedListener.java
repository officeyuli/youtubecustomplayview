package com.backup;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;

public interface CustomerOnInitializedListener {
    void onInitializationSuccess(YouTubePlayer.Provider provider,YouTubePlayer player,Boolean wasRestored);
    void onInitializationFailure(YouTubePlayer.Provider provider,YouTubeInitializationResult result);
}
